module ApplicationHelper

 def formattedErrors(model)
   errors_details = model.errors.details
   all_errors = []
   errors_details.each do |field, errors|
     errors.each do |error|
       formatted_error = nil
       if error[:error] == :blank #如果为空
         formatted_error = Error.new("missing_field", field)
       elsif error[:error] == :taken #如果已经存在
         formatted_error = Error.new("duplicated_field", field)
       elsif error[:error] == :too_long #too_long
         formatted_error = Error.new("field_is_too_long", field)
       elsif error[:error] == :too_short #too_short
         formatted_error = Error.new("field_is_too_short", field)
       else #其余全是invalide
         formatted_error = Error.new("invalide_field", field)
       end
       all_errors << formatted_error
     end
   end
   all_errors
 end
end
