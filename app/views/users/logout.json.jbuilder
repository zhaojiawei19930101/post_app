json.logout_info do
    json.user_id @user.id
    json.email @user.email
end