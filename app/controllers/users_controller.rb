class UsersController < ApplicationController
  before_action :require_login, only:[:show]
  def show
    @user = User.find_by(id: params[:id])
    if @user
      render 'show.json.jbuilder', status: :ok
    else
      head :not_found
    end
  end
  
  private def require_login
    unless $redis.exists(params[:key])
      @errors = [Error.new("require_login", "key")]
      render 'shared/errors.json.jbuilder', status: :bad_request
    end
  end
  
  def create
    @user = User.new(user_params)
    if @user.save 
      render "show.json.jbuilder", status: :ok
    else
      @errors = formattedErrors(@user)
      #respond_to do |format| 
        render 'shared/errors.json.jbuilder', status: :bad_request
      #end
    end
  rescue ActionController::ParameterMissing => e
    @errors = [Error.new("missing_field", "user")]
    render 'shared/errors.json.jbuilder', status: :bad_request
  end
  
  def login
    user_email = login_params[:email]
    @user = User.find_by(email: user_email)
    if @user
      if @user.authenticate(login_params[:password])
        @user_token = Digest::SHA256.hexdigest(user_email) + Digest::SHA256.hexdigest(Time.zone.now.to_s)[0..9]
        $redis.set(@user_token, @user.id)
        @expire_in = 3600*24*30
        $redis.expire(@user_token, @expire_in)
        render 'login.json.jbuilder', status: :ok
      else
        @errors = [Error.new("incorrect_password", "password")]
        render 'shared/errors.json.jbuilder', status: :bad_request
      end
    else
      @errors = [Error.new("email_not_found", "email")]
      render 'shared/errors.json.jbuilder', status: :not_found
    end
    
  rescue ActionController::ParameterMissing => e
    @errors = [Error.new("missing_field", "user")]
    render 'shared/errors.json.jbuilder', status: :bad_request
  end
  
  def logout
    key = logout_params[:key]
    if key
      $redis.del(key)
      head :ok
    else
      @errors = [Error.new("missing_field", "key")]
      render 'shared/errors.json.jbuilder', status: :bad_request
    end
  
  rescue ActionController::ParameterMissing => e
    @errors = [Error.new("missing_field", "user")]
    render 'shared/errors.json.jbuilder', status: :bad_request
  end
  private def user_params
    params.require(:user).permit(:email, :password, :password_confirmation)
  end
  
  private def login_params
    params.require(:user).permit(:email, :password)
  end
  
  private def logout_params
    params.require(:user).permit(:user_id, :key)
  end
end
