class PostsController < ApplicationController

  # GET domain/posts
  def index
    all_posts = Post.all
     respond_to do |format|
        format.json {render json: all_posts, status: :ok}
     end
  end
  
  # GET domain/posts/:id
  def show
    post_by_id = Post.find_by(id: params[:id])
    if post_by_id
      respond_to do |format|
        format.json {render json: post_by_id, status: :ok}
      end
    else
        head :not_found
    end
  end
  
  # POST domain/posts/
  def create
    new_post = Post.new(post_params)
    if new_post.save
      render json: new_post, status: :ok
    else
      @errors = formattedErrors(new_post)
      #respond_to do |format| 
        render 'shared/errors.json.jbuilder', status: :bad_request
      #end
    end
    
  rescue ActionController::ParameterMissing => e
    @errors = [Error.new("missing_field", "post")]
     #respond_to do |format|
      render 'shared/errors.json.jbuilder', status: :bad_request
    #end
    
  end
  
  private def post_params
    params.require(:post).permit(:title, :content)
  end
end
