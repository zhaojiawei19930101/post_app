class CommentsController < ApplicationController
  
  def index
    one_post = Post.find_by(id: params[:post_id])

    if one_post
      comments_by_post_id = one_post.comments
      respond_to do |format|
        format.json {render json: comments_by_post_id, status: :ok}
      end
    else
      head :not_found
    end
    
  end
  
  def show
    one_comment = Comment.find_by(id: params[:id])
    if one_comment
      respond_to do |format|
        format.json {render json: one_comment, status: :ok}
      end
    else
      head :not_found
    end
  end
  
  def create
    #new_comment = Comment.new(content: comment_params[:content], post_id: params[:id])
    post_id = params[:post_id]
    one_post = Post.find_by(id: post_id)
    if one_post
      new_comment = Comment.new(content: comment_params[:content], post_id: post_id)
      
      if new_comment.save
        render json: new_comment, status: :ok
      else
        @errors = formattedErrors(new_comment)
        render 'shared/errors.json.jbuilder', status:  :bad_request
      end
    else
      head :not_found
    end
    
  rescue ActionController::ParameterMissing => e
    @errors = [Error.new("missing_field", "comment")]
    render 'shared/errors.json.jbuilder', status: :bad_request
  end
  
  
  private def comment_params
    params.require(:comment).permit(:content)
  end
end
