require 'test_helper'

class PostTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  
  def setup
    @post = Post.new(title: "@*255", content: "Post Content")
  end
  
  test "should be valid" do
    assert @post.valid?
  end
  
  test 'title should be present' do
    @post.title = "     "
    assert_not @post.valid?
  end
  
  test 'title should not be too long' do
    @post.title = "@"*256
    assert_not @post.valid?
  end
  
end
