require 'test_helper'

class UserActionsTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  def setup
    $redis.flushall
  end
  test "create a user which is valid" do
    assert_difference 'User.count', 1 do
      post users_path, user: { email: "Example@gmail.com", password: "123456", password_confirmation: "123456"}
    end
    assert_template 'show.json.jbuilder'
    assert_response :ok
  end
  
  test "cannot create user without :user field" do
    assert_no_difference 'User.count' do
      post users_path
    end
    assert_template 'shared/errors.json.jbuilder'
    assert_response :bad_request
  end
  
  test "cannot create a invalid user" do
    assert_no_difference 'User.count' do
      post users_path, user:{email: "@"*256, password: "123456"*3, password_confirmation: "23456"*3}
    end
    assert_template 'shared/errors.json.jbuilder'
    assert_response :bad_request
  end  
  
# login test
  test "cannot login without :user field" do
    assert_no_difference '$redis.keys.count' do
      post '/user_tokens'
    end
    assert_template 'shared/errors.json.jbuilder'
    assert_response :bad_request
  end
  
  test "cannot login with a non-existing email" do
    assert_no_difference '$redis.keys.count' do
      post '/user_tokens', user: {email: "examplegmail.com", password:"123456"}
    end
    assert_template 'shared/errors.json.jbuilder'
    assert json_response == {'errors'=>[{'code'=>'email_not_found','field'=>'email'}]}
    assert_response :not_found
  end
  
  test "cannot login with an incorrect password" do
    assert_no_difference '$redis.keys.count' do
      post '/user_tokens', user: {email: "user1@gmail.com", password:"123456"}
    end
    assert_template 'shared/errors.json.jbuilder'
    assert json_response == {'errors'=>[{'code'=>'incorrect_password','field'=>'password'}]}
    assert_response :bad_request
  end  
  
  def successfully_login
    post '/user_tokens', user: {email: "user1@gmail.com", password:"user1@gmail.com"}
  end
  test "login in successfully" do
    #email = "user1@gmail.com"
    #u = users(:one)
    #post '/user_tokens', user: {email: "user1@gmail.com", password:"user1@gmail.com"}
    successfully_login
    assert $redis.exists(json_response['key']) 
    assert_template 'login.json.jbuilder'
    assert_response :ok
  end   
  
  #logout
  test "cannot logout successfully without user field" do
    successfully_login
    assert_no_difference '$redis.keys.count' do
      post '/logout'
    end
    assert_template 'shared/errors.json.jbuilder'
    assert json_response == {'errors'=>[{'code'=>'missing_field','field'=>'user'}]}
    assert_response :bad_request
  end
  
  test "cannot logout successfully without a key" do
    successfully_login
    assert_no_difference '$redis.keys.count' do
      post '/logout', user:{user_id: json_response['user_id']}
    end
    assert_template 'shared/errors.json.jbuilder'
    assert json_response == {'errors'=>[{'code'=>'missing_field','field'=>'key'}]}
    assert_response :bad_request
  end
  
  test "logout successfully" do
    successfully_login
    user_id = json_response['user_id']
    key = json_response['key']
    post '/logout', user:{user_id: user_id, key: key}
    assert_not $redis.exists(key)
    assert_response :ok
  end
  
  #show a user (require login test)
  
  test "cannot show a user without a key" do
    # successfully_login
    # user_id = json_response['user_id']
    # key = json_response['key']
    get user_path(1)
    assert_template 'shared/errors.json.jbuilder'
    assert_response :bad_request
  end

  test "cannot show a user with a non-existing key" do
    successfully_login
    user_id = json_response['user_id']
    key = json_response['key']
    get user_path(user_id) + "?key = 123"
    assert_template 'shared/errors.json.jbuilder'
    assert_response :bad_request
  end  
  
  test "can show a user with a existing key" do
    successfully_login
    user_id = json_response['user_id']
    key = json_response['key']
    url = user_path(user_id) + "?key=#{key}"
    get url
    assert_template 'show.json.jbuilder'
    assert json_response == {"user"=>{"user_id"=>1, "email"=>"user1@gmail.com"}}
    assert_response :ok
  end
end
