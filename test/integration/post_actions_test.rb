require 'test_helper'

class PostActionsTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end

  
  test "get posts" do
    get posts_path + '.json'
    assert_response :success
  end
  
  test "show a post that can be found" do
    get post_path(1) + '.json'
    assert_response :ok
  end
  
  test "return 404 for a not found post" do
    get post_path(5) + '.json'
    assert_response :not_found
  end
  
  test "create a post which is valid" do
    assert_difference 'Post.count', 1 do
      post posts_path, post: { title: "Example Post", content: "content" }
    end
    assert_response :ok
  end
  
  test "cannot create an invalid post (blank title)" do
    assert_no_difference 'Post.count' do
      post posts_path, post: { title: "   ", content: "content" }
    end
    assert_template 'shared/errors.json.jbuilder'
    assert_response :bad_request
  end
  
  test "cannot create an invalid post (title is too long)" do
    assert_no_difference 'Post.count' do
      post posts_path, post: { title: "@"*256, content: "content" }
    end
    assert_template 'shared/errors.json.jbuilder'
    assert_response :bad_request
  end
  
  test "cannot create a post without the :post field)" do
    assert_no_difference 'Post.count' do
      post posts_path
    end
    assert_template 'shared/errors.json.jbuilder'
    assert_response :bad_request
  end
end
