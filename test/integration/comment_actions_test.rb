require 'test_helper'

class CommentActionsTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end

  
  test "get comments for a existing post" do
    get '/posts/1/comments' + '.json'
    assert_response :ok
  end
  
    test "cannot find comments for a non-existing post" do
    get '/posts/5/comments' + '.json'
    assert_response :not_found
  end
  
  test "show a comment that can be found" do
    get comment_path(1) + '.json'
    assert_response :ok
  end
  
  test "return 404 for a not found comment" do
    get comment_path(5) + '.json'
    assert_response :not_found
  end
  
  test "cannot create a comment for a non-existing post" do
    assert_no_difference 'Comment.count' do
      post '/posts/5/comments', comment: { content: "comment content" }
    end
    assert_response :not_found
  end
 
  test "cannot create a comment without the :comment field" do
    assert_no_difference 'Comment.count' do
      post '/posts/1/comments'
    end
    assert_response :bad_request
  end 
  
  test "create a comment for a existing post" do
    assert_difference 'Comment.count', 1 do
      post '/posts/1/comments', comment: {content: 'comment content'}
    end
    assert_response :ok
  end 
end
